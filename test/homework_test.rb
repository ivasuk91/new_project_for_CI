require 'minitest/autorun'
require './homework1'

class HomeworkTest < Minitest::Test
  describe 'global checking' do
    it 'returns a string' do
      generate.must_be_kind_of String
    end
  end
end
