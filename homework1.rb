VARIANT = 8
LETTERS = *('A'..'Z')

def generate
  letter = -> { LETTERS.sample(3).join }
  number = -> { rand(100..999).to_s }

  [letter, number, letter, number].map(&:call).join('-')
end
